﻿// Ogólnie ten program to jakaś magia i nie mam pojęcia jak tego dokonałem ale działa.
// Nie ruszać!!! W razie problemów z działaniem programu wyrzucić komputer/laptop za okno!

using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Szyfrowanie
{
    public interface IEncrypting
    {
        
    }
    static class Info
    {
        public static char[] AlphabetLetters = {'A', 'Ą', 'B',
                                                'C', 'Ć', 'D',
                                                'E', 'Ę', 'F',
                                                'G', 'H', 'I',
                                                'J', 'K', 'L',
                                                'Ł', 'M', 'N',
                                                'Ń', 'O', 'Ó',
                                                'P', 'Q', 'R',
                                                'S', 'Ś', 'T',
                                                'U', 'V', 'W',
                                                'X', 'Y', 'Z',
                                                'Ź', 'Ż', '!',
                                                '@', '#', '$',
                                                '%', '^', '&',
                                                '*', '(', ')',
                                                '-', '_', '=',
                                                '+', '[', '{',
                                                ']', '}', ';',
                                                ':', '"', '\'',
                                                '\\', '|', ',',
                                                '<', '.', '>',
                                                '/', '?', '1',
                                                '2', '3', '4',
                                                '5', '6', '7',
                                                '8', '9', '0'}; // Alphabet
        public static string path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)+ @"/Szyfr Vigenèrea";
    }
    public class Menu : IEncrypting
    {
        char[] AlphabetLetters = Info.AlphabetLetters;
        static int index = 0;
        private void SaveTable()
        {
            Console.Clear();
            int left = Console.WindowWidth / 2;
            int top = Console.WindowHeight / 2;
            if (File.Exists(Info.path+@"/CryptTable.txt"))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Plik z tablicą znaków już istnieje!");
                Console.ResetColor();
                Console.WriteLine();
            }
            else
            {
                StreamWriter table = File.CreateText(Info.path + @"/CryptTable.txt");
                char[,] CryptTable = new char[AlphabetLetters.Length, AlphabetLetters.Length];
                for (int i = 0; i < AlphabetLetters.Length; i++)
                {
                    for (int j = 0; j < AlphabetLetters.Length; j++)
                    {
                        CryptTable[i, j] = AlphabetLetters[(j + i) % AlphabetLetters.Length];
                    }
                }
                for (int i = 0; i < AlphabetLetters.Length; i++)
                {
                    for(int j = 0; j < AlphabetLetters.Length; j++)
                    {
                        table.Write(CryptTable[i, j] + " ");
                    }
                    table.WriteLine();
                }
                table.Close();
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                Console.WriteLine("Plik został utworzony");
                Console.ResetColor();
                Console.WriteLine();
            }
            Console.WriteLine("Naciśnij dowolny klawisz, żeby wrócić do menu...");
            Console.ReadKey();
            Program start = new Program();
        }
        private void SaveToFile(string[] data)
        {
            if (File.Exists(Info.path+@"/SaveFile.txt"))
            {
                string[] s = File.ReadAllLines(Info.path + @"/SaveFile.txt");
                for (int i = 0; i < s.Length; i++)
                {
                    data = data.Concat(new string[] { s[i] }).ToArray();
                }
                File.WriteAllLines(Info.path + @"/SaveFile.txt", data);
            }
            else
            {
                File.WriteAllLines(Info.path + @"/SaveFile.txt", data);
            }
            Console.WriteLine("Juhu! Zapis wykonany!");
            System.Threading.Thread.Sleep(2000);
            Program start = new Program();

        }
        public void ShowMenu(List<string> menuItems, int id, string title, string[] data = null)
        {
            Historic historic = new Historic(); // Creating link to Historic Class
            Original original = new Original(); // Creating link to Original Class
            Console.CursorVisible = false;
            bool menuW = true;
            while (menuW)
            {
                Console.WriteLine(title);
                Console.WriteLine("----------------");
                string selectedMenuItem = DrawMenu(menuItems);
                switch (selectedMenuItem)
                {
                    case " Historyczny szyfr ":
                        menuW = false;
                        historic.HistoricMenu();
                        break;
                    case " Oryginalny szyfr ":
                        menuW = false;
                        original.OriginalMenu();
                        break;
                    case " Szyfruj ":
                        switch (id)
                        {
                            case 1:
                                menuW = false;
                                historic.Encrypting();
                                break;
                            case 2:
                                menuW = false;
                                original.Encrypting();
                                break;
                            default:
                                Console.WriteLine("Błąd programu: ID wybranego elementu jest niepoprawne!");
                                Console.ReadKey();
                                Environment.Exit(0);
                                break;
                        }
                        break;
                    case " Deszyfruj ":
                        switch (id)
                        {
                            case 1:
                                menuW = false;
                                historic.Decrypting();
                                break;
                            case 2:
                                menuW = false;
                                original.Decrypting();
                                break;
                            default:
                                Console.WriteLine("Błąd programu: ID wybranego elementu jest niepoprawne!");
                                Console.ReadKey();
                                Environment.Exit(0);
                                break;
                        }
                        break;
                    case " Zapisz dane do pliku ":
                        menuW = false;
                        SaveToFile(data);
                        break;
                    case " Otwórz folder zapisu ":
                        Console.Clear();
                        System.Diagnostics.Process.Start(Info.path);
                        break;
                    case " Wróć ":
                        menuW = false;
                        Program start = new Program();
                        break;
                    case " Zapisz tablicę znaków ":
                        menuW = false;
                        SaveTable();
                        break;
                    case " Wyjście ":
                        Environment.Exit(0);
                        break;
                }
            }
        }
        private static string DrawMenu(List<string> items)
        {
            for (int i = 0; i < items.Count; i++)
            {
                if (i == index)
                {
                    Console.BackgroundColor = ConsoleColor.DarkGreen;
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine(items[i]);
                }
                else
                {
                    Console.WriteLine(items[i]);
                }
                Console.ResetColor();
            }
            ConsoleKeyInfo ckey = Console.ReadKey();

            if (ckey.Key == ConsoleKey.DownArrow)
            {
                if (index == items.Count - 1)
                {
                    index = 0;
                }
                else
                {
                    index++;
                }
            }
            else if (ckey.Key == ConsoleKey.UpArrow)
            {
                if (index <= 0)
                {
                    index = items.Count - 1;
                }
                else
                {
                    index--;
                }
            }
            else if (ckey.Key == ConsoleKey.Enter)
            {
                return items[index];
            }
            else
            {
                //return "";
            }
            Console.Clear();
            return "";
        }
    }
    public class Historic : IEncrypting
    {
        char[] AlphabetLetters = Info.AlphabetLetters;
        private char CreateCryptogram(char col,char row)
        {
            int x = AlphabetLetters.Length;
            char[,] CryptTable = new char[x, x];
            Console.Clear();
            for (int i = 0; i < x; i++)
            {
                for (int j = 0; j < x; j++)
                {
                    CryptTable[i, j] = AlphabetLetters[(j + i) % x];
                }
            }
            for (int i = 0; i < x; i++)
            {
                for (int j = 0; j < x; j++)
                {
                    if (CryptTable[0, j] == col)
                    {
                        if(CryptTable[i, 0] == row)
                        {
                            return CryptTable[i, j];
                        }
                    }
                }
            }
            return '?';
        }
        public void Encrypting()
        {
            Console.Clear();
            Console.Write("Podaj tekst który chcesz zaszyfrować: ");
            string OText = Console.ReadLine();
            char[] OTextLetters = OText.ToUpper().ToCharArray();
            Console.Write("Wpisz hasło bezpieczeństwa: ");
            string Pass = Console.ReadLine().Replace(" ", string.Empty).ToUpper();
            char[] PassLetters = new char[0];
            while (Pass.Length < OText.Length)
            {
                Pass += Pass;
            }
            int Cr=0;
            for(int i = 0; i < OTextLetters.Length; i++)
            {
                if (OTextLetters[i] == ' ')
                {
                    PassLetters = PassLetters.Concat(new char[] { ' ' }).ToArray();
                    if (Cr == 0)
                    {
                        Cr = i;
                    }
                    else
                    {
                        PassLetters = PassLetters.Concat(new char[] { Pass[Cr] }).ToArray();
                        Cr = i;
                    }
                }
                else
                {
                    if (Cr != 0)
                    {
                        PassLetters = PassLetters.Concat(new char[] { Pass[Cr] }).ToArray();
                        Cr = i;
                    }
                    else
                    {
                        PassLetters = PassLetters.Concat(new char[] { Pass[i] }).ToArray();
                    }
                }
                //Console.WriteLine(Cr);
                //Console.ReadKey();
            }
            
            char[] CryptogramLetters = new char[1];
            for(int i = 0; i < OTextLetters.Length; i++)
            {
                if (OTextLetters[i] != ' ')
                {
                    CryptogramLetters = CryptogramLetters.Concat(new char[] { CreateCryptogram(OTextLetters[i], PassLetters[i]) }).ToArray();
                } else
                {
                    CryptogramLetters = CryptogramLetters.Concat(new char[] { ' ' }).ToArray();
                }
            }
            string Cryptogram = new string(CryptogramLetters);
            Menu menu = new Menu();
            List<string> menuItems = new List<string>()
            {
                " Zapisz dane do pliku ",
                " Wróć ",
                " Wyjście "
            };
            string Data = "Historyczny - ";
            Data += DateTime.Now.ToString("dddd, dd MMMM yyyy HH:mm:ss");
            string[] data = new string[] { "------", Data, "------" };
            data = data.Concat(new string[] { "Oryginalny tekst: " + OText, "Hasło: " + Pass, "Kryptogram:" + Cryptogram }).ToArray();
            menu.ShowMenu(menuItems,1, "Twój zaszyfrowany tekst: "+Cryptogram,data);
        }
        private char CreateOriginalText(char row, char end)
        {
            int x = AlphabetLetters.Length;
            char[,] CryptTable = new char[x, x];
            Console.Clear();
            int r = 0;
            for (int i = 0; i < x; i++)
            {
                for (int j = 0; j < x; j++)
                {
                    CryptTable[i, j] = AlphabetLetters[(j + i) % x];
                    if (CryptTable[i, 0] == row)
                    {
                        r = i;
                        if (CryptTable[r, j] == end)
                        {
                            return AlphabetLetters[j];
                        }
                    }
                }
            }
            return '?';
        }
        public void Decrypting()
        {
            Console.Clear();
            Console.Write("Wpisz szyfrogram: ");
            string Cryptogram = Console.ReadLine();
            char[] CryptogramLetters = Cryptogram.ToUpper().ToCharArray();
            Console.Write("Wpisz hasło: ");
            string Pass = Console.ReadLine().Replace(" ", string.Empty).ToUpper();
            char[] PassLetters = new char[0];
            while (Pass.Length < Cryptogram.Length)
            {
                Pass += Pass;
            }
            int Cr = 0;
            for (int i = 0; i < CryptogramLetters.Length; i++)
            {
                if (CryptogramLetters[i] == ' ')
                {
                    PassLetters = PassLetters.Concat(new char[] { ' ' }).ToArray();
                    if (Cr == 0)
                    {
                        Cr = i;
                    }
                    else
                    {
                        PassLetters = PassLetters.Concat(new char[] { Pass[Cr] }).ToArray();
                        Cr = i;
                    }
                }
                else
                {
                    if (Cr != 0)
                    {
                        PassLetters = PassLetters.Concat(new char[] { Pass[Cr] }).ToArray();
                        Cr = i;
                    }
                    else
                    {
                        PassLetters = PassLetters.Concat(new char[] { Pass[i] }).ToArray();
                    }
                }
                //Console.WriteLine(Cr);
                //Console.ReadKey();
            }
            
            char[] OriginalTextLetters = new char[0];
            for(int i = 0; i < Cryptogram.Length; i++)
            {
                if(CryptogramLetters[i] != ' ')
                {
                    OriginalTextLetters = OriginalTextLetters.Concat(new char[] { CreateOriginalText(PassLetters[i], CryptogramLetters[i]) }).ToArray();
                } else
                {
                    OriginalTextLetters = OriginalTextLetters.Concat(new char[] { ' ' }).ToArray();
                }
            }
            string OriginalText = new string(OriginalTextLetters);
            Menu menu = new Menu();
            List<string> menuItems = new List<string>()
            {
                " Zapisz dane do pliku ",
                " Wróć ",
                " Wyjście "
            };
            string Data = "Historyczny - ";
            Data += DateTime.Now.ToString("dddd, dd MMMM yyyy HH:mm:ss");
            string[] data = new string[] { "------", Data, "------" };
            data = data.Concat(new string[] { "Zaszyfrowany tekst: " + Cryptogram, "Hasło: " + Pass, "Oryginał: " + OriginalText }).ToArray();
            menu.ShowMenu(menuItems, 1, "Twój odszyfrowany tekst: " + OriginalText, data);
        }
        public void HistoricMenu()
        {
            Console.Clear();
            Menu menu = new Menu();
            List<string> menuItems = new List<string>()
            {
                " Szyfruj ",
                " Deszyfruj ",
                " Wróć "
            };
            menu.ShowMenu(menuItems, 1, "Szyfr Vigenère’a - Historyczny");
        }
    } // ID: 1
    public class Original : IEncrypting //ID: 2
    {
        char[] AlphabetLetters = Info.AlphabetLetters;

        private char CreateCryptogram(char col,char row)
        {
            int x = AlphabetLetters.Length;
            char[,] CryptTable = new char[x, x];
            Console.Clear();
            for (int i = 0; i < x; i++)
            {
                for (int j = 0; j < x; j++)
                {
                    CryptTable[i, j] = AlphabetLetters[(j + i) % x];
                }
            }
            for (int i = 0; i < x; i++)
            {
                for (int j = 0; j < x; j++)
                {
                    if (CryptTable[0, j] == col)
                    {
                        if (CryptTable[i, 0] == row)
                        {
                            return CryptTable[i, j];
                        }
                    }
                }
            }
            return '?';
        }
        public void Encrypting()
        {
            Console.Clear();
            Console.Write("Podaj tekst który chcesz zaszyfrować: ");
            string OText = Console.ReadLine();
            char[] OTextLetters = OText.ToUpper().ToCharArray();
            Console.Write("Podaj literę szyfrującą(dowolna): ");
            string Pass = Console.ReadLine().Replace(" ", string.Empty).ToUpper();
            char[] PassLetters = new char[1];
            PassLetters[0] = Pass[0];
            int Cr = 0;

            for (int i = 0; i < OTextLetters.Length - 1; i++)
            {
                if (OTextLetters[i + 1] == ' ')
                {
                    PassLetters = PassLetters.Concat(new char[] { ' ' }).ToArray();
                    if (Cr == 0)
                    {
                        Cr = i;
                    }
                    else
                    {
                        PassLetters = PassLetters.Concat(new char[] { OTextLetters[Cr] }).ToArray();
                        Cr = i;
                    }
                }
                else
                {
                    if (Cr != 0)
                    {
                        PassLetters = PassLetters.Concat(new char[] { OTextLetters[Cr] }).ToArray();
                        Cr = 0;
                    }
                    else
                    {
                        PassLetters = PassLetters.Concat(new char[] { OTextLetters[i] }).ToArray();
                    }
                }
            }

            char[] CryptogramLetters = new char[0];
            for (int i = 0; i < OTextLetters.Length; i++)
            {
                if (OTextLetters[i] != ' ')
                {
                    CryptogramLetters = CryptogramLetters.Concat(new char[] { CreateCryptogram(OTextLetters[i], PassLetters[i]) }).ToArray();
                }
                else
                {
                    CryptogramLetters = CryptogramLetters.Concat(new char[] { ' ' }).ToArray();
                }
            }
            string Cryptogram = new string(CryptogramLetters);
            Menu menu = new Menu();
            List<string> menuItems = new List<string>()
            {
                " Zapisz dane do pliku ",
                " Wróć ",
                " Wyjście "
            };
            string Data = "Oryginalny - ";
            Data += DateTime.Now.ToString("dddd, dd MMMM yyyy HH:mm:ss");
            string[] data = new string[] { "------", Data, "------" };
            data = data.Concat(new string[] { "Oryginalny tekst: " + OText, "Litera szyfrująca: " + Pass[0], "Kryptogram: " + Cryptogram }).ToArray();
            menu.ShowMenu(menuItems, 1, "Twój zaszyfrowany tekst: " + Cryptogram, data);
        }
        private char CreateOriginalText(char row, char end)
        {
            //Console.WriteLine("Row: "+row+" End: "+end);
            //Console.ReadKey();
            int x = AlphabetLetters.Length;
            char[,] CryptTable = new char[x, x];
            Console.Clear();
            int r = 0;
            for (int i = 0; i < x; i++)
            {
                for (int j = 0; j < x; j++)
                {
                    CryptTable[i, j] = AlphabetLetters[(j + i) % x];
                    if (CryptTable[i, 0] == row)
                    {
                        r = i;
                        if (CryptTable[r, j] == end)
                        {
                            return AlphabetLetters[j];
                        }
                    }
                }
            }
            return '?';
        }
        public void Decrypting()
        {
            Console.Clear();
            Console.Write("Wpisz szyfrogram: ");
            string Cryptogram = Console.ReadLine();
            char[] CryptogramLetters = Cryptogram.ToUpper().ToCharArray();
            Console.Write("Wpisz hasło: ");
            string Pass = Console.ReadLine().Replace(" ", string.Empty).ToUpper();
            char[] PassLetters = new char[] { Pass[0] };
            char[] OriginalTextLetters = new char[0];
            
            int Cr = 0;
            for (int i = 0; i < CryptogramLetters.Length - 1; i++)
            {
                if (CryptogramLetters[i + 1] == ' ')
                {
                    PassLetters = PassLetters.Concat(new char[] { ' ' }).ToArray();
                    if (Cr == 0)
                    {
                        Cr = i;
                    }
                    else
                    {
                        PassLetters = PassLetters.Concat(new char[] { CreateOriginalText(PassLetters[Cr], CryptogramLetters[Cr]) }).ToArray();
                        Cr = i;
                    }
                }
                else
                {
                    if (Cr != 0)
                    {
                        PassLetters = PassLetters.Concat(new char[] { CreateOriginalText(PassLetters[Cr], CryptogramLetters[Cr]) }).ToArray();
                        Cr = 0;
                    }
                    else
                    {
                        PassLetters = PassLetters.Concat(new char[] { CreateOriginalText(PassLetters[i], CryptogramLetters[i]) }).ToArray();
                    }
                }
            }
            
            for (int i = 0; i < CryptogramLetters.Length; i++)
            {
                if (CryptogramLetters[i] != ' ')
                {
                    OriginalTextLetters = OriginalTextLetters.Concat(new char[] { CreateOriginalText(PassLetters[i], CryptogramLetters[i]) }).ToArray();
                }
                else
                {
                    OriginalTextLetters = OriginalTextLetters.Concat(new char[] { ' ' }).ToArray();
                }
            }
            string OriginalText = new string(OriginalTextLetters);
            Menu menu = new Menu();
            List<string> menuItems = new List<string>()
            {
                " Zapisz dane do pliku ",
                " Wróć ",
                " Wyjście "
            };
            string Data = "Oryginalny - ";
            Data += DateTime.Now.ToString("dddd, dd MMMM yyyy HH:mm:ss");
            string[] data = new string[] { "------", Data, "------" };
            data = data.Concat(new string[] { "Zaszyfrowany tekst: " + Cryptogram, "Litera szyfrująca: " + Pass[0], "Oryginał: " + OriginalText }).ToArray();
            Console.Clear();
            menu.ShowMenu(menuItems, 1, "Twój odszyfrowany tekst: " + OriginalText, data);
        }
        public void OriginalMenu()
        {
            Console.Clear();
            Menu menu = new Menu();
            List<string> menuItems = new List<string>()
            {
                " Szyfruj ",
                " Deszyfruj ",
                " Wróć "
            };
            menu.ShowMenu(menuItems, 2, "Szyfr Vigenère’a - Oryginalny");

        }
    } // ID: 2
    class Program //ID: 0
    {
        public Program()
        {
            Console.Clear();
            Main();
        }
        static void Main()
        {
            Console.SetWindowSize(120, 40);
            Console.Title = "Szyfr Vigenère’a - Piotr Wasielewski - v.1.2.1";
            if (!Directory.Exists(Info.path))
            {
                Directory.CreateDirectory(Info.path);
            }
            Menu menu = new Menu();
            List<string> menuItems = new List<string>()
            {
                " Historyczny szyfr ",
                " Oryginalny szyfr ",
                " Zapisz tablicę znaków ",
                " Otwórz folder zapisu ",
                " Wyjście "
            };
            menu.ShowMenu(menuItems, 0, "Szyfr Vigenère’a");
            Console.WriteLine("Oof! Wyszedłeś z pętli czasoprzestrzennej!"); // Jeśli coś z programem pójdzie nie tak to zostanie wyświetlona ta wiadomość
            Console.Read();
        }
    }
}

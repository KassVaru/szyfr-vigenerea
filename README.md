# Szyfr Vigenère’a

# > Projekt wykonany na podstawie zadania:

> Szyfrowanie wieloalfabetyczne (polialfabetyczne) polega na tworzeniu
> wielu alfabetów służących do utworzenia szyfrogramu.
> Jednym z ciekawych algorytmów jest szyfr Vigenere'a.
> Jak tworzone są alfabety dla podanego wyrazu (zdania) szyfrującego?
> Algorytm ( program), który dla podanego wyrazu 
> utworzy nam alfabety szyfrujące Vigenere'a.

# > Przed wyruszeniem w drogę należy zebrać drużynę
* [Visual Studio 2017](https://visualstudio.microsoft.com/pl/downloads/)

# > Pokaż mi swoje towary!
Jest kilka opcji:

1. > Kod źródłowy włączamy w programie Visual Studio
2. > W folderze `Szyfrowanie/bin/Debug/` znajduje się plik Szyfr_Vigenère’a.exe, > pozwala on na uruchomienie programu bez potrzeby posiadania kompilatora.
3. > Instalujemy program z obrazu ISO umieszczonego w plikach projektu lub z tego [linku](https://actaeon.one.pl/uploads/Szyfr/Szyfr_Vigenère’a_-_latest.iso)

# > Zmiany są efektem postępu
* > Poprzednie wersje programu możesz znaleźć [tutaj](https://actaeon.one.pl/uploads/Szyfr/)

## v1.2.1
    + Dodano: Opcje otwarcia folderu z zapisanymi plikami (tablica szyfrująca/zaszyfrowane wiadomości)
    * Poprawiono: Zapis odbywa się w folderze Dokumenty w osobnym folderze

## v1.2.0
    + Dodano: Zapis do pliku tablicy szyfrującej
    + Dodano: Skalowanie okna konsoli
    + Dodano: Statyczną klasę dla alfabetu
    + Dodano: Znaki specjalne oraz liczby w alfabecie
    * Poprawiono: Zarządzanie alfabetem w kodzie programu
    - Usunięto: Wyświetlanie tablicy szyfrującej
## v1.0.1
    * Poprawiono: Informacje o programie w systemie Windows
    * Poprawiono: Nazwa programu w systemie Windows
    * Poprawiono: Zapis informacji do plików
## v1.0.0
    + Dodano: Menu główne
    + Dodano: Szyfrowanie i deszyfrowanie z możliwością wyboru sposobu (oryginalny/historyczny)
    + Dodano: Możliwość wyświetlenia tabeli szyfrującej
    + Dodano: Opcje wyjścia/powrotu do menu
    + Dodano: Zapis szyfrowania i deszyfrowania do pliku tekstowego